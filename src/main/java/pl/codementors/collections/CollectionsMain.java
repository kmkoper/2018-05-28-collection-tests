package pl.codementors.collections;

import pl.codementors.collections.models.Dragon;
import pl.codementors.collections.models.DragonList;

public class CollectionsMain {
    public static void main(String[] args) {
        DragonList dl = new DragonList();
        DragonList dragonList;
        Dragon d1, d2, d3, d4, d5;
        dragonList = new DragonList();
        d1 = new Dragon("Antipodean Opaleye", 50, "red", 15);
        d2 = new Dragon("Common Welsh Green", 123, "green", 25);
        d3 = new Dragon("Norwegian Ridgeback Brown", 76, "brown", 27);
        d4 = new Dragon("Antipodean Opaleye", 50, "red", 15);
        d5 = d2;
        dl.addDragon(d1);
        dl.addDragon(d2);
        dl.addDragon(d3);
        dl.addDragon(d4);
        dl.addDragon(d5);
        System.out.println(dl.getOldestDragon());
        System.out.println(dl.getWidestRangeOfWings());
        System.out.println(dl.getValueOfLongestName());

    }
    private static void prepare() {

    }
}
