package pl.codementors.collections.models;

import java.util.Objects;

public class Dragon {
    private String  name = null;
    private int age = 0;
    private String color = null;
    private int rangeOfWings = 0;

    public Dragon(String name, int age, String color, int rangeOfWings) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.rangeOfWings = rangeOfWings;
    }

    public Dragon() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getRangeOfWings() {
        return rangeOfWings;
    }

    public void setRangeOfWings(int rangeOfWings) {
        this.rangeOfWings = rangeOfWings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dragon dragon = (Dragon) o;
        return age == dragon.age &&
                rangeOfWings == dragon.rangeOfWings &&
                Objects.equals(name, dragon.name) &&
                Objects.equals(color, dragon.color);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, age, color, rangeOfWings);
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                ", rangeOfWings=" + rangeOfWings +
                '}';
    }
}
