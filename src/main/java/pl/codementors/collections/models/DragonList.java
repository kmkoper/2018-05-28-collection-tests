package pl.codementors.collections.models;

import java.util.ArrayList;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


import static java.util.Collections.unmodifiableList;


public class DragonList {
    private final List<Dragon> dragons = new ArrayList<>();

    /**
     * Add dragon to list.
     * @param dragon
     */
    public void addDragon(Dragon dragon){
        dragons.add(dragon);
    }

    /**
     * Delete dragon from list.
     * @param dragon
     */
    public void deleteDragon(Dragon dragon){
        dragons.remove(dragon);
    }

    /**
     *
     * @return unmodifiable list of all dragons.
     */
    public List<Dragon> getDragons() {
        return unmodifiableList(dragons);
    }

    /**
     * @return oldest Dragon.
     */
    public Dragon getOldestDragon(){
        return dragons.stream().max(Comparator.comparingInt(Dragon::getAge)).get();

    }

    /**
     * @return value of widest range of dragons wings.
     */
    public int getWidestRangeOfWings(){
        return (dragons.stream().max(Comparator.comparingInt(Dragon::getRangeOfWings))
                .get()).getRangeOfWings();
    }

    /**
     * @return number of letters in dragons longest name.
     */
    public int getValueOfLongestName(){
        return (dragons.stream().max(Comparator.comparingInt(d -> d.getName().length())).get())
                .getName().length();
    }

    /**
     * @return list of dragons sorted by its age
     */
    public List<Dragon> getAgeSortedDragons(){
       return unmodifiableList(dragons.stream().sorted(Comparator.comparingInt(Dragon::getAge)).collect(Collectors.toList()));

    }

    /**
     * @param color
     * @return list of color specified dragons
     */
    public List<Dragon> getColorSpecifiedDragonList(String color){
        return dragons.stream().filter(value -> value.getColor().equals(color)).collect(Collectors.toList());
    }

    /**
     * @return list of dragons names.
     */
    public List<String> getListWithOnlyNames() {
       return dragons.stream().map(Dragon::getName).collect(Collectors.toList());
    }

    /**
     * @return list of dragons colors.
     */
    public List<String> getListWithOnlyColorsUpperCase(){
       return dragons.stream().map(item -> item.getColor().toUpperCase()).collect(Collectors.toList());
    }

    /**
     *
     * @param age
     * @return true when all dragons are older than param.
     */
    public boolean checkIfAllAreOlder(int age) {
        return dragons.stream().allMatch(item -> item.getAge()> age);
    }

    /**
     *
     * @param color
     * @return true when there is dragon in this color.
     */
    public boolean checkIfThereIsDragonInThatColor(String color) {
        return dragons.stream().anyMatch(item -> item.getColor().equals(color));
    }
}
