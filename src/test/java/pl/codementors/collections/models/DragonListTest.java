package pl.codementors.collections.models;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

public class DragonListTest {
    DragonList dragonList;
    Dragon d1, d2, d3, d4;

    @Rule
    public ExpectedException exceptionGrabber = ExpectedException.none();

    @Before
    public void prepare() {
        dragonList = new DragonList();
        d1 = new Dragon("Antipodean Opaleye", 50, "red", 15);
        d2 = new Dragon("Common Welsh Green", 123, "green", 25);
        d3 = new Dragon("Norwegian Ridgeback Brown", 76, "brown", 27);
        d4 = new Dragon("Antipodean Opaleye", 50, "red", 15);
        dragonList.addDragon(d1);
        dragonList.addDragon(d2);
        dragonList.addDragon(d3);
        dragonList.addDragon(d4);
    }


    @Test
    public void getOldestDragonTest() {
        Assert.assertEquals(123, dragonList.getOldestDragon().getAge());
    }

    @Test
    public void getWidestRangeOfWingsTest() {
        Assert.assertEquals(27, dragonList.getWidestRangeOfWings());
    }

    @Test
    public void getValueOfLongestName() {
        Assert.assertEquals(25, dragonList.getValueOfLongestName());
    }

    @Test
    public void getAgeSortedDragons() {
        List<Dragon> ageSorted = dragonList.getAgeSortedDragons();
        Assert.assertTrue(ageSorted.indexOf(d3) == 2);
    }

    @Test
    public void getColorSpecifiedDragonsCheckingSameColor() {
        List<Dragon> colorSpecified = dragonList.getColorSpecifiedDragonList("red");
        Assert.assertTrue(colorSpecified.contains(d1) && colorSpecified.contains(d4));
    }

    @Test
    public void getColorSpecifiedDragonsCheckingDifferentColor() {
        List<Dragon> colorSpecified = dragonList.getColorSpecifiedDragonList("red");
        Assert.assertFalse(colorSpecified.contains(d2));
    }

    @Test
    public void getListWithOnlyNames() {
        List<String> listOfNames = dragonList.getListWithOnlyNames();
        Assert.assertTrue("There should be that dragon", listOfNames.contains("Common Welsh Green"));
        Assert.assertFalse("There should be no color here", listOfNames.contains("red"));
    }

    @Test
    public void getListWithOnlyColorsWhenCheckingUpperCase() {
        List<String> listOfColors = dragonList.getListWithOnlyColorsUpperCase();
        Assert.assertTrue("There should be color", listOfColors.contains("RED"));
    }

    @Test
    public void getListWithOnlyColorsWhenCheckingLowerCase() {
        List<String> listOfColors = dragonList.getListWithOnlyColorsUpperCase();
        Assert.assertFalse("There shouldn't find color writed in lower case letters", listOfColors.contains("red"));
    }
    @Test
    public void checkIfAllAreOlderWhenReallyOlder(){
        Assert.assertTrue("This should be true", dragonList.checkIfAllAreOlder(1));
    }
    @Test
    public void checkIfAllAreOlderWhenSomeAreYounger() {
        Assert.assertFalse("This should be false", dragonList.checkIfAllAreOlder(80));
    }

    @Test
    public void checkIfThereIsDragonInThatColorWhenThereIsDragonInThisColor(){
        Assert.assertTrue("There should be dragon in this color",
                dragonList.checkIfThereIsDragonInThatColor("red"));
    }
    @Test
    public void checkIfThereIsDragonInThatColorWhenThereIsNoDragonInThisColor(){
        Assert.assertFalse("There should not be that dragon here!",
                dragonList.checkIfThereIsDragonInThatColor("yellow"));
    }
}
