package pl.codementors.collections.models;

import org.junit.Assert;
import org.junit.Test;

public class DragonTest {
    @Test
    public void DragonWhenEverythingProvided(){
       Dragon dragon1 = new Dragon("Smok", 2, "czerwony",5);
        Assert.assertNotNull("There is no dragon!", dragon1);
    }
    @Test
    public void DragonWhenUsingEmptyConstructor(){
        Dragon dragon = new Dragon();
        Assert.assertNull("There should be unnamed dragon", dragon.getName());
    }
    @Test
    public void equalsWhenDifferentReferencesSameValue(){
        Dragon dragon1 = new Dragon("Smok", 2, "czerwony",5);
        Dragon dragon2 = new Dragon("Smok", 2, "czerwony",5);
        boolean test = dragon1.equals(dragon2);
        Assert.assertTrue("They should be the same", test);

    }
    @Test
    public void equalsWhenDifferentValue(){
        Dragon dragon1 = new Dragon("Smok", 2, "czerwony",5);
        Dragon dragon2 = new Dragon("komS", 2, "niebieski",30);
        boolean test = dragon1.equals(dragon2);
        Assert.assertFalse("They should be different", test);
    }
    @Test
    public void equalsWhenTheSameReferences(){
        Dragon dragon1 = new Dragon("Smok", 2, "czerwony",5);
        Dragon dragon2 = dragon1;
        boolean test = dragon1.equals(dragon2);
        Assert.assertTrue("objects should be the same", test);
    }
    @Test
    public void hashCodeWhenTheSameValue(){
        Dragon dragon1 = new Dragon("Smok", 2, "czerwony",5);
        Dragon dragon2 = new Dragon("Smok", 2, "czerwony",5);
        int hash1 = dragon1.hashCode();
        int hash2 = dragon2.hashCode();
        Assert.assertTrue("hashCode should be the same", hash1 == hash2);
    }
}
